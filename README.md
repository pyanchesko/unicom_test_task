####Запуск:

    docker-compose build
    docker-compose up -d
    docker-compose exec server ./manage.py migrate


####Документация: 
  - http://127.0.0.1:8000/redoc/ 
  - http://127.0.0.1:8000/swagger/
