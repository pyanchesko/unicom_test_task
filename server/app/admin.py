from django.contrib import admin

from .models import *


@admin.register(Offer)
class OfferAdmin(admin.ModelAdmin):
    list_display = ('name', 'offer_type', 'min_scoring', 'max_scoring', 'credit_organisation')
    readonly_fields = ('created_at', 'updated_at')
    list_filter = ('offer_type', 'credit_organisation')
    fields = ('created_at', 'updated_at', 'name', 'offer_type', 'min_scoring', 'max_scoring')


@admin.register(ClientForm)
class ClientFormAdmin(admin.ModelAdmin):
    list_display = ('name', 'scoring', 'partner')
    readonly_fields = ('created_at', 'updated_at')
    list_filter = ('partner',)

    def name(self, obj):
        return obj


@admin.register(ApplicationForm)
class ApplicationFormAdmin(admin.ModelAdmin):
    list_display = ('client_form', 'offer', 'status')
    readonly_fields = ('created_at', 'updated_at')
    list_filter = ('status', 'offer',)


class UserAdmin(admin.ModelAdmin):
    list_display = ('credit_organisation', 'partner', 'status')


admin.site.register(CreditOrganisation)
admin.site.register(Partner)
admin.site.register(User)
