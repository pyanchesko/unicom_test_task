from django_filters import rest_framework as filters
from .models import ClientForm, ApplicationForm


class ClientFormFilter(filters.FilterSet):
    birthday = filters.DateFromToRangeFilter()
    min_scoring = filters.NumberFilter(field_name="scoring", lookup_expr='gte')
    max_scoring = filters.NumberFilter(field_name="scoring", lookup_expr='lte')

    class Meta:
        model = ClientForm
        fields = ['min_scoring', 'max_scoring', 'birthday']


class ApplicationFormFilter(filters.FilterSet):
    client_birthday = filters.DateFromToRangeFilter(field_name='client_form__birthday')
    client_min_scoring = filters.NumberFilter(field_name="client_form__scoring", lookup_expr='gte')
    client_max_scoring = filters.NumberFilter(field_name="client_form__scoring", lookup_expr='lte')
    client_partner = filters.NumberFilter(field_name='client_form__partner')
    offer_type = filters.NumberFilter(field_name='offer__offer_type')
    offer_minimal_scoring = filters.NumberFilter(field_name='offer__scoring', lookup_expr='gte')
    offer_maximal_scoring = filters.NumberFilter(field_name='offer__scoring', lookup_expr='lte')

    class Meta:
        model = ApplicationForm
        fields = [
            'client_min_scoring', 'client_max_scoring', 'client_birthday', 'client_partner',
            'offer_type', 'offer__min_scoring', 'offer__max_scoring'
            ]
