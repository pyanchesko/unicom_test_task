from django.contrib.auth.models import AbstractUser
from django.db import models


class OfferType:
    consumer = 1
    mortgage = 2
    auto_loan = 3


OFFER_TYPE_CHOICES = (
    (OfferType.consumer, 'потребительский'),
    (OfferType.mortgage, 'ипотека'),
    (OfferType.auto_loan, 'автокредит'),
)


class StatusType:
    new = 1
    sent = 2
    taken = 3
    approved = 4
    denied = 5
    given = 6


STATUS_TYPE_CHOICES = (
    (StatusType.new, 'новая'),
    (StatusType.sent, 'отправлена'),
    (StatusType.taken, 'получена'),
    (StatusType.approved, 'одобрена'),
    (StatusType.denied, 'отказано'),
    (StatusType.given, 'отдана'),
)


class CreditOrganisation(models.Model):
    name = models.CharField(max_length=200, verbose_name='Название')

    class Meta:
        verbose_name = 'Кредитная организация'
        verbose_name_plural = 'Кредитные организации'

    def __str__(self):
        return self.name


class Partner(models.Model):
    name = models.CharField(max_length=200, verbose_name='Название')

    class Meta:
        verbose_name = 'Партнер'
        verbose_name_plural = 'Партнеры'

    def __str__(self):
        return self.name


class User(AbstractUser):
    # TODO: по-хорошему, нужно сделать constraint на поля - может быть заполнено только одно
    credit_organisation = models.ForeignKey(CreditOrganisation, on_delete=models.CASCADE,
                                            blank=True, null=True, verbose_name='кредитная организация')
    partner = models.ForeignKey(Partner, on_delete=models.CASCADE, blank=True, null=True, verbose_name='партнер')

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return self.username


class Offer(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='последнее изменение')
    start_rotation_at = models.DateTimeField(blank=True, null=True)
    finished_rotation_at = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=200, verbose_name='Название')
    offer_type = models.IntegerField(choices=OFFER_TYPE_CHOICES, verbose_name='тип предложения')
    min_scoring = models.IntegerField(verbose_name='минимальный скоринговый балл')
    max_scoring = models.IntegerField(verbose_name='максимальный скоринговый балл')
    credit_organisation = models.ForeignKey(CreditOrganisation, on_delete=models.CASCADE,
                                            verbose_name='кредитная организация')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Предложение'
        verbose_name_plural = 'Прдложения'


class ClientForm(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='последнее изменение')
    last_name = models.CharField(max_length=100, verbose_name='фамилия')
    first_name = models.CharField(max_length=100, verbose_name='имя')
    patronymic = models.CharField(max_length=100, verbose_name='отчество')
    birthday = models.DateField(verbose_name='дата рождения')
    phone_number = models.CharField(max_length=10, verbose_name='номер телефона')
    passport_number = models.CharField(max_length=10, verbose_name='номер паспорта')
    scoring = models.IntegerField(verbose_name='скоринговый балл')
    partner = models.ForeignKey(Partner, on_delete=models.CASCADE, verbose_name='парнер')

    def __str__(self):
        return '{} {} {}'.format(self.first_name, self.last_name, self.patronymic)

    class Meta:
        verbose_name = 'Анкета клиента'
        verbose_name_plural = 'Анкеты клиентов'


class ApplicationForm(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='последнее изменение')
    client_form = models.ForeignKey(ClientForm, on_delete=models.CASCADE, verbose_name='анкета клиента')
    offer = models.ForeignKey(Offer, on_delete=models.CASCADE, verbose_name='предложение')
    status = models.IntegerField(choices=STATUS_TYPE_CHOICES, default=StatusType.new, verbose_name='статус')

    def __str__(self):
        return '{} - {}'.format(self.client_form, self.offer)

    class Meta:
        verbose_name = 'Заявка в кредитную организацию'
        verbose_name_plural = 'Заявки в кредитные организациюи'
