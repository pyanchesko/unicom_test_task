from rest_framework.permissions import BasePermission, SAFE_METHODS


class SuperuserOrPartner(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_superuser or request.user.partner


class ClientFormPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            if request.user.is_superuser:
                return True

            return request.user.partner == obj.partner

        return request.user.is_superuser


class ApplicationFormListPermission(BasePermission):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return request.user.is_superuser or request.user.partner or request.user.credit_organisation

        return request.user.is_superuser or request.user.partner


class ApplicationFormPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            if request.user.partner:
                return obj.client_form.partner == request.user.partner
            if request.user.credit_organisation:
                return obj.offer.credit_organisation == request.user.credit_organisation

            return request.user.is_superuser

        if request.method == 'PATCH':
            if request.user.is_superuser or request.user.credit_organisation:
                return True
        if request.user.is_superuser:
            return True
