from rest_framework.serializers import ModelSerializer, SerializerMethodField

from .models import ClientForm, ApplicationForm, Offer, CreditOrganisation, Partner


class CreditOrganisationSerializer(ModelSerializer):

    class Meta:
        model = Partner
        fields = ('id', 'name',)
        read_only_fields = ('id', 'name',)


class PartnerSerializer(ModelSerializer):

    class Meta:
        model = CreditOrganisation
        fields = ('id', 'name',)
        read_only_fields = ('id', 'name',)


class ClientFormCreateSerializer(ModelSerializer):

    class Meta:
        model = ClientForm
        fields = ('id', 'last_name', 'first_name', 'patronymic', 'birthday',
                  'phone_number', 'passport_number', 'scoring', 'partner')
        read_only_fields = ('id',)


class ClientFormSerializer(ModelSerializer):
    partner = PartnerSerializer()

    class Meta:
        model = ClientForm
        fields = ('id', 'last_name', 'first_name', 'patronymic', 'birthday',
                  'phone_number', 'passport_number', 'scoring', 'partner')
        read_only_fields = ('id',)


class OfferSerializer(ModelSerializer):
    offer_type = SerializerMethodField()
    credit_organisation = CreditOrganisationSerializer()

    class Meta:
        model = Offer
        fields = (
            'id', 'start_rotation_at', 'finished_rotation_at', 'name', 'offer_type',
            'min_scoring', 'max_scoring', 'credit_organisation',
        )

    def get_offer_type(self, obj):
        return obj.get_offer_type_display()


class ApplicationFormSerializer(ModelSerializer):
    offer = OfferSerializer(read_only=True)
    client_form = ClientFormSerializer(read_only=True)
    status = SerializerMethodField(read_only=False)

    class Meta:
        model = ApplicationForm
        fields = ('id', 'status', 'offer', 'client_form')
        read_only_fields = ('id', 'created_at', 'updated_at', 'offer', 'client_form')

    def get_status(self, obj):
        return obj.get_status_display()


class ChangeStatusSerializer(ModelSerializer):

    class Meta:
        model = ApplicationForm
        fields = ('status',)


class ApplicationFormCreateSerializer(ModelSerializer):

    class Meta:
        model = ApplicationForm
        fields = ('id', 'client_form', 'offer')
        read_only_fields = ('id',)
