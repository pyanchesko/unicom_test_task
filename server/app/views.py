from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView
from rest_framework.filters import OrderingFilter
from rest_framework.permissions import IsAuthenticated

from .filters import ClientFormFilter, ApplicationFormFilter
from .models import ClientForm, ApplicationForm, Offer
from .permissions import (SuperuserOrPartner, ClientFormPermission, ApplicationFormListPermission,
                          ApplicationFormPermission,)
from .serializers import (ClientFormSerializer, ApplicationFormSerializer, ApplicationFormCreateSerializer,
                          OfferSerializer, ClientFormCreateSerializer, ChangeStatusSerializer)


class ClientFormListCreate(ListCreateAPIView):
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filterset_class = ClientFormFilter
    ordering_fields = ('created_at', 'updated_at', 'birthday', 'scoring', 'last_name')
    permission_classes = (IsAuthenticated, SuperuserOrPartner)

    def get_queryset(self):
        if self.request.user.is_superuser:
            return ClientForm.objects.all()
        if self.request.user.partner:
            return ClientForm.objects.filter(partner=self.request.user.partner)

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return ClientFormSerializer
        else:
            return ClientFormCreateSerializer


class ClientFormView(RetrieveUpdateDestroyAPIView):
    serializer_class = ClientFormSerializer
    queryset = ClientForm.objects.all()
    permission_classes = (IsAuthenticated, ClientFormPermission,)


class ApplicationFormListCreate(ListCreateAPIView):
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filterset_class = ApplicationFormFilter
    ordering_fields = ('created_at', 'updated_at',)
    permission_classes = (IsAuthenticated, ApplicationFormListPermission,)

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return ApplicationFormSerializer
        else:
            return ApplicationFormCreateSerializer

    def get_queryset(self):
        if self.request.user.is_superuser:
            return ApplicationForm.objects.all()
        if self.request.user.partner:
            return ApplicationForm.objects.filter(client_form__partner=self.request.user.partner)
        if self.request.user.credit_organisation:
            return ApplicationForm.objects.filter(offer__credit_organisation=self.request.user.credit_organisation)


class ApplicationFormView(RetrieveUpdateDestroyAPIView):
    queryset = ApplicationForm.objects.all()
    permission_classes = (IsAuthenticated, ApplicationFormPermission,)

    def get_serializer_class(self):
        if self.request.method == 'PATCH':
            return ChangeStatusSerializer
        return ApplicationFormSerializer


class OfferListView(ListAPIView):
    serializer_class = OfferSerializer
    permission_classes = (IsAuthenticated, SuperuserOrPartner,)

    def get_queryset(self):
        client = ClientForm.objects.get(pk=self.kwargs.get('pk', 0))
        return Offer.objects.filter(min_scoring__lte=client.scoring, max_scoring__gte=client.scoring)
